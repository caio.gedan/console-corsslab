package pt.sibs.cross_console.base

import androidx.appcompat.app.AppCompatActivity
import pt.sibs.first_cross.base.executor.IExecutorScope

/**
 * Created by csilva at 06/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
abstract class BaseActivity : AppCompatActivity() {
    protected abstract val vm: Array<IExecutorScope>

    override fun onDestroy() {
        vm.forEach { it.cancel() }
        super.onDestroy()
    }
}