package pt.sibs.cross_console.di

import org.koin.core.module.Module
import org.koin.dsl.module
import pt.sibs.first_cross.features.document.mvi.DocumentDetailViewModel
import pt.sibs.first_cross.features.document.mvi.DocumentsViewModel

/**
 * Created by csilva at 06/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */

val viewModelModule: Module = module {
    single { DocumentsViewModel() }
    single { DocumentDetailViewModel() }
}