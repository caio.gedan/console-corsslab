package pt.sibs.cross_console.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import pt.sibs.cross_console.databinding.FragmentStartBinding
import pt.sibs.cross_console.ui.components.DocumentsListAdapter
import pt.sibs.cross_console.ui.components.ManagerResourceState
import pt.sibs.first_cross.features.document.mvi.DocumentDetailViewModel
import pt.sibs.first_cross.features.document.mvi.DocumentsContract
import pt.sibs.first_cross.features.document.mvi.DocumentsViewModel
import pt.sibs.first_cross.services.CppBridge
import pt.sibs.first_cross.services.provider.IServiceProvider

/**
 * Created by csilva at 05/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */

class FragmentStart : Fragment() {

    private lateinit var binding: FragmentStartBinding
    private val viewModel: DocumentDetailViewModel by inject()
    private val viewModel2: DocumentsViewModel by inject()

    private val docAdapter = DocumentsListAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {

        binding = FragmentStartBinding.inflate(layoutInflater, container, false)
        binding.docList.layoutManager = LinearLayoutManager(context)
        binding.docList.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )

        binding.docList.adapter = docAdapter

        setListeners()

        return binding.root
    }

    private fun setListeners() {
        binding.btnNovoDocumento.setOnClickListener(::onClickNew)

        binding.btnClearConsole.setOnClickListener(::onClick)

        //Listen State Change (Documents List)
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel2.uiState.collect { data ->
                    ManagerResourceState(
                        resourceState = data.docListState,
                        successView = { doc ->
                            requireNotNull(doc)
                            docAdapter.addDocuments(doc)
                        },
                        onTryAgain = { viewModel2.setEvent(DocumentsContract.Event.OnGetDocuments) },
                        onCheckAgain = { viewModel2.setEvent(DocumentsContract.Event.OnGetDocuments) },
                    )
                }
            }
        }
    }

    private fun onClick(v: View) {
//        viewModel2.setEvent(DocumentsContract.Event.OnGetDocuments)

        CppBridge.startTest()
    }

    val testService: IServiceProvider by inject()
    private fun onClickNew(v: View) {
//        viewModel.setEvent(DocumentDetailContract.Event.AssignDocument)
    }

}