package pt.sibs.cross_console.ui.components

import pt.sibs.cross_console.ui.components.state.Empty
import pt.sibs.cross_console.ui.components.state.Error
import pt.sibs.cross_console.ui.components.state.Loading
import pt.sibs.first_cross.base.mvi.BasicUiState

/**
 * Created by csilva at 08/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
fun <T> ManagerResourceState(
    resourceState: BasicUiState<T>,
    successView: (data: T?) -> Unit,
    onTryAgain: () -> Unit,
    msgTryAgain: String = "No data to present!",
    onCheckAgain: () -> Unit,
    msgCheckAgain: String = "An error has occurred!"
) {

    when (resourceState) {
        is BasicUiState.Empty -> Empty(onCheckAgain = onCheckAgain, msg = msgCheckAgain)
        is BasicUiState.Error -> resourceState.message?.let { Error(onTryAgain = onTryAgain, msg = it) }
        BasicUiState.Loading -> Loading()
        is BasicUiState.Success -> successView(resourceState.data)
        BasicUiState.Idle -> Unit
    }

}