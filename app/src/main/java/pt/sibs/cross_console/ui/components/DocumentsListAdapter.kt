package pt.sibs.cross_console.ui.components

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import pt.sibs.cross_console.R
import pt.sibs.cross_console.databinding.ListItemDocBinding
import pt.sibs.first_cross.domain.model.Document

/**
 * Created by csilva at 10/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
class DocumentsListAdapter : RecyclerView.Adapter<DocumentsListAdapter.DocItemViewHolder>() {
    private val docList = ArrayList<Document>()
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DocItemViewHolder {
        val binding = ListItemDocBinding.inflate(LayoutInflater.from(parent.context))

        context = parent.context
        return DocItemViewHolder(binding)
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onBindViewHolder(holder: DocItemViewHolder, position: Int) {
        holder.imageView.background = context.getDrawable(R.drawable.ic_launcher_background)
        holder.docTitle.text = docList[position].name
        holder.docStatus.text = docList[position].status.name
    }

    override fun getItemCount(): Int = docList.size

    fun addDocuments(items: List<Document>) {
        val oSize = docList.size
        items.forEach { doc ->
            if (!docList.contains(doc)) {
                docList.add(doc)
                notifyItemChanged(oSize, items.size)
            }
        }
    }

    class DocItemViewHolder(binding: ListItemDocBinding) : RecyclerView.ViewHolder(binding.root) {
        var imageView: AppCompatImageView = binding.imageView
        var docTitle: AppCompatTextView = binding.docTitle
        var docStatus: AppCompatTextView = binding.docStatus
    }

}