package pt.sibs.cross_console.ui

import android.os.Bundle
import org.koin.android.ext.android.inject
import pt.sibs.cross_console.R
import pt.sibs.cross_console.base.BaseActivity
import pt.sibs.cross_console.databinding.ActivityMainBinding
import pt.sibs.first_cross.base.executor.IExecutorScope
import pt.sibs.first_cross.features.document.mvi.DocumentDetailViewModel
import pt.sibs.first_cross.features.document.mvi.DocumentsViewModel

class MainActivity : BaseActivity() {

    private val vmDocuments: DocumentsViewModel by inject()
    private val vmDocumentDetail: DocumentDetailViewModel by inject()

    override val vm: Array<IExecutorScope>
        get() = arrayOf(vmDocuments, vmDocumentDetail)

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)

        supportFragmentManager.beginTransaction().add(
            R.id.container,
            FragmentStart()
        ).commit()
    }

}