package pt.sibs.cross_console.ui.components.state

import android.util.Log

/**
 * Created by csilva at 08/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
fun Loading() {

    Log.i("APP", "Loading...!")

}