package pt.sibs.cross_console

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.logger.Level
import pt.sibs.cross_console.di.viewModelModule
import pt.sibs.first_cross.BuildConfig
import pt.sibs.first_cross.di.startDI

/**
 * Created by csilva at 06/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
class CrossApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startDI {
            androidLogger(if (BuildConfig.DEBUG) Level.ERROR else Level.NONE)
            androidContext(this@CrossApplication)
            modules(
                viewModelModule
            )
        }
    }
}